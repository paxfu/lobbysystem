package at.paxfu.mct.listeners;

import at.paxfu.mct.LobbySystem;
import at.paxfu.mct.managers.lobbyInventory;
import at.paxfu.mct.managers.regionEnter;
import at.paxfu.mct.messages;
import de.zetor.global.apis.MCTPlayerManager;
import de.zetor.global.enums.Stats_Enums;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.sql.SQLException;

public class playerDeath implements Listener {

    @EventHandler
    public static void onDeath(PlayerDeathEvent event) {
        event.getDrops().clear();
        Player p = event.getEntity();
        if (regionEnter.knockbackFFA.contains(p)) {
            if (event.getEntity().getKiller() == null) {
                //Add Death
                try {
                    MCTPlayerManager.setPlayerStats(Stats_Enums.LOBBY_DEATHS, p, Integer.parseInt(MCTPlayerManager.getPlayerStats(Stats_Enums.LOBBY_DEATHS, p)) + 1);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                //SetPoints
                try {
                    MCTPlayerManager.setPlayerStats(Stats_Enums.LOBBY_POINTS, p, Integer.parseInt(MCTPlayerManager.getPlayerStats(Stats_Enums.LOBBY_POINTS, p)) - 2);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                p.sendMessage(messages.PREFIX + " §7LobbyMinigame >> §cYou died! §7(§c-2 Points§7)");
            } else {
                //Add Death
                try {
                    MCTPlayerManager.setPlayerStats(Stats_Enums.LOBBY_DEATHS, p, Integer.parseInt(MCTPlayerManager.getPlayerStats(Stats_Enums.LOBBY_DEATHS, p)) + 1);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                //Add Kill
                try {
                    MCTPlayerManager.setPlayerStats(Stats_Enums.LOBBY_KILLS, p, Integer.parseInt(MCTPlayerManager.getPlayerStats(Stats_Enums.LOBBY_KILLS, p.getKiller())) + 1);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                //SetPoints
                try {
                    MCTPlayerManager.setPlayerStats(Stats_Enums.LOBBY_POINTS, p, Integer.parseInt(MCTPlayerManager.getPlayerStats(Stats_Enums.LOBBY_POINTS, p)) - 3);
                    MCTPlayerManager.setPlayerStats(Stats_Enums.LOBBY_POINTS, p.getKiller(), Integer.parseInt(MCTPlayerManager.getPlayerStats(Stats_Enums.LOBBY_POINTS, p.getKiller())) + 5);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                p.sendMessage(messages.PREFIX + " §7LobbyMinigame >> You got killed by §a " + p.getKiller().getDisplayName() + " §7(§c-3 Points§7)");
                p.getKiller().sendMessage(messages.PREFIX + " §7LobbyMinigame >> You killed §a" + p.getDisplayName() + " §7(§a+5 Points§7)");
                p.getKiller().playSound(p.getKiller().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1F, 1F);
            }
            event.setDeathMessage("");
            p.spigot().respawn();
            p.getInventory().clear();
            lobbyInventory.setInventory(p);

            double x = LobbySystem.locations.getDouble("Kffa.X");
            double y = LobbySystem.locations.getDouble("Kffa.Y");
            double z = LobbySystem.locations.getDouble("Kffa.Z");
            float yaw = (float) LobbySystem.locations.getDouble("Kffa.YAW");
            float pitch = (float) LobbySystem.locations.getDouble("Kffa.PITCH");
            World world = Bukkit.getWorld(LobbySystem.locations.getString("Kffa.WORLD"));

            Location location = new Location(world, x, y, z, yaw, pitch);
            p.teleport(location);

        }
    }
}
