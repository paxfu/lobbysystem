package at.paxfu.mct;

import at.paxfu.mct.commands.buildCMD;
import at.paxfu.mct.commands.gmCMD;
import at.paxfu.mct.commands.setspawnCMD;
import at.paxfu.mct.listeners.*;
import at.paxfu.mct.managers.regionEnter;
import at.paxfu.mct.utils.hotbarScheduler;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class LobbySystem extends JavaPlugin {

    public static File configf, langf, locationsf;
    public static FileConfiguration config, lang, locations;

    @EventHandler
    public void onEnable() {

        instance = this;
        registerEvents();
        registerCommands();
        createFiles();

        joinListener.scoreboardScheduler();
        hotbarScheduler.start();

    }

    public void registerEvents() {
        Bukkit.getPluginManager().registerEvents(new joinListener(), this);
        Bukkit.getPluginManager().registerEvents(new quitListener(), this);
        Bukkit.getPluginManager().registerEvents(new onPlace(), this);
        Bukkit.getPluginManager().registerEvents(new onBreak(), this);
        Bukkit.getPluginManager().registerEvents(new onDrop(), this);
        Bukkit.getPluginManager().registerEvents(new entityDamage(), this);
        Bukkit.getPluginManager().registerEvents(new onClick(), this);
        Bukkit.getPluginManager().registerEvents(new inventoryClick(), this);
        Bukkit.getPluginManager().registerEvents(new playerMove(), this);
        Bukkit.getPluginManager().registerEvents(new playerDeath(), this);

        Bukkit.getPluginManager().registerEvents(new regionEnter(), this);
    }
    public void registerCommands() {
        getCommand("setspawn").setExecutor(new setspawnCMD());
        getCommand("build").setExecutor(new buildCMD());
        getCommand("gm").setExecutor(new gmCMD());
    }

    //Files
    private void createFiles(){
        configf = new File(getDataFolder(), "config.yml");
        langf = new File(getDataFolder(), "lang.yml");
        locationsf = new File(getDataFolder(), "locations.yml");

        if (!configf.exists()){
            configf.getParentFile().mkdirs();
            saveResource("config.yml", false);
        }

        if (!langf.exists()){
            langf.getParentFile().mkdirs();
            saveResource("lang.yml", false);
        }

        if (!locationsf.exists()){
            locationsf.getParentFile().mkdirs();
            saveResource("locations.yml", false);
        }

        config = new YamlConfiguration();
        lang = new YamlConfiguration();
        locations = new YamlConfiguration();

        try {
            config.load(configf);
            lang.load(langf);
            locations.load(locationsf);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }




    public static LobbySystem instance;

    public static LobbySystem getInstance() {
        return instance;
    }
}
